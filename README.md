CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The Freegeoip Views module mainly provides views contextual filter plugin with
options such as ip, country code, region_code, region_name, city, zip_code,
time_zone, latitude, longitude, metro_code and services for getting whole
Freegeoip details and for getting a particular value of the same based of user
ip address.

REQUIREMENTS
------------

This module has no dependency requirements.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
  https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure
  for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.

MAINTAINERS
-----------

Current maintainers:
* Rakesh James (rakesh.gectcr) - https://www.drupal.org/u/rakeshgectcr
* Manoj Kumar (manojapare) - https://www.drupal.org/u/manojapare
